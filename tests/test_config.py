#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
import unittest
import shutil
import json
import os
from smart_remover.utility.config import get_extension, get_dict_config_from_json_file
import datetime
from smart_remover.utility.policy import AutocleanType

WORK_DIR = os.path.join(os.getcwd(), "BucketTests")
CONFIG_FILE = os.path.join(WORK_DIR, 'CONFIG_FILE')


def string_iso_from_datetime(datetime_for_write):
    return datetime.datetime.strftime(datetime_for_write, '%Y-%m-%dT%H:%M:%S.%f')


DEFAULT_CONFIG = {
    'bucket': {
        'bucket_dir': os.path.join(os.environ['HOME'], '.smart_rm/trash'),
        'trashfiles_dir_name': 'files',
        'infofile': 'trashfile.info',

        'max_bucket_size_in_bytes': 1024*1024*1024
    },
    'mode': {
        'silent': False,
        'interactive': False,
        'verbose': False,
        'recursive': False,
        'directory': False,
        'dry_run': False
    },
    #'action_type':Action.remove,
    'logger':{
        'log_file': os.path.join(os.environ['HOME'], '.smart_rm/log.file'),
        'log_level': "DEBUG",
        'log_in_console': True
    },
    #'files': None,
    'policy': {
        'earliest_deletion_time': datetime.datetime(2000, 1,1),
        'autoclean_type': AutocleanType.no_autoclean
    }
}

class ConfigTestCase(unittest.TestCase):
    def setUp(self):
        os.makedirs(WORK_DIR)

    def tearDown(self):
        shutil.rmtree(WORK_DIR)

    def test_get_extension(self):
        self.assertEqual(get_extension('file.ini'), '.ini')
        self.assertEqual(get_extension('file.json'), '.json')

    def test_get_dict_config_from_json_parser_file(self):
        default = lambda obj: (obj.isoformat() if isinstance(obj, datetime.datetime) else None)
        with open(CONFIG_FILE, 'w') as config_file:
            json.dump(DEFAULT_CONFIG, config_file, indent=4, default=default)#, default=json_util.default)

        config_from_file = get_dict_config_from_json_file(CONFIG_FILE)
        self.assertDictEqual(DEFAULT_CONFIG, config_from_file)

if __name__ == '__main__':
    unittest.main()
