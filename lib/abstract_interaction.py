"""Module with bucket abstract infrastructure classes

This module classes provide interfaces for inheritance in classes, which will use
in bucket as infrastructure.

"""
from abc import abstractmethod
from abc import ABCMeta


class ResultWriter(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def write_removed(self, removed_bucket_item):
        pass

    @abstractmethod
    def write_restored(self, removed_bucket_item):
        pass

    @abstractmethod
    def write_clear(self, removed_bucket_item):
        pass

    @abstractmethod
    def write_clean(self):
        pass


class Querist(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def want_del_file(self, file_name):
        pass

    @abstractmethod
    def enter_dir(self, dir_name):
        pass

    @abstractmethod
    def want_del_empty_dir(self, dir_name):
        pass

    @abstractmethod
    def want_clear_all(self):
        pass

    @abstractmethod
    def want_clear_by_index(self,index):
        pass

    @abstractmethod
    def want_clear_by_basename(self,base_name):
        pass

    @abstractmethod
    def want_restore_by_basename(self, base_name):
        pass

    @abstractmethod
    def want_restore_by_index(self, index):
        pass

#
# class ProgressWriter(object):
#     __metaclass__ = ABCMeta
#
#     @abstractmethod
#     def write(self, done, total):
#         pass
#
