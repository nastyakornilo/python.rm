# -*- coding: utf-8 -*-
"""This module contain errors that can be raised while smart bucket work"""
import errno
import os


class PermissionError(EnvironmentError):
    """Permission error - user doesn't have write rights to access the file

    Args:
        filename (str): Name of file, to which it tries to access
        msg (str): Human readable string describing the exception.

    """

    def __init__(self, filename="", msg=""):
        super(PermissionError, self).__init__(errno.EACCES, os.strerror(errno.EACCES)+msg,filename)


class NotEnoughFreeSpaceException(Exception):
    """Not enough free space to replace file in bucket

    Args:
        filename (str): Name of file, which it tries to remove
        msg (str): Human readable string describing the exception.

    """

    def __init__(self, filename="", msg=""):
        super(NotEnoughFreeSpaceException,
              self).__init__('Not enough free space in bucket. Can not remove {} '.format(filename)+msg)


class RemovingBucketParentDirectoryError(Exception):
    """Error occur while trying remove to bucket directory, which is parent(higher-level) directory
    for bucket.

        Args:
            filename (str): Name of directory, which it tries to remove
            msg (str): Human readable string describing the exception.

        """

    def __init__(self,filename="", msg=""):
        super(RemovingBucketParentDirectoryError, self).__init__(
            "Can't move in bucket directory witch is higher is file system{}".format(filename) + msg)


class PathBelongBucketError(Exception):
    """Error occur while trying damage bucket necessary files

        Args:
            filename (str): Name of file, which it tries to damage
            msg (str): Human readable string describing the exception.

        """

    def __init__(self,filename="", msg=""):
        super(PathBelongBucketError, self).__init__("Can't remove {}. Path belong to bucket".format(filename) + msg)


class BucketDoesNotContainItemError(Exception):
    """Error occur while trying access item that bucket does not contain

        Args:
            item_data (str): data needed for accessing the item
            msg (str): Human readable string describing the exception.

        """

    def __init__(self, item_data="", msg=""):
        super(BucketDoesNotContainItemError,
              self
              ).__init__("Bucket does not contain item, item data:{}, {}".format(item_data, msg))


class PathExistenceError(EnvironmentError):
    """Error occur while accessing path that does not exist

        Args:
            filename (str): Name of file, which it tries to access
            msg (str): Human readable string describing the exception.

        """

    def __init__(self, filename="", msg=""):
        super(PathExistenceError, self).__init__(errno.ENOENT, os.strerror(errno.ENOENT)+msg, filename)


class PathIsDirectoryError(EnvironmentError):
    """Error occur while trying to remove directory in non-directory mode

        Args:
            dirname (str): Name of directory, which it tries to remove
            msg (str): Human readable string describing the exception.

        """

    def __init__(self, dirname="", msg=""):
        super(PathIsDirectoryError, self).__init__(errno.EISDIR, os.strerror(errno.EISDIR)+msg, dirname)

#
# class PathIsNotDirectoryError(EnvironmentError):
#     """Error occur while trying to remove directory in non-directory mode
#
#             Args:
#                 filename (str): Name of file, which it tries to remove
#                 msg (str): Human readable string describing the exception.
#
#             """
#
#     def __init__(self, filename="", msg=""):
#         super(PathIsNotDirectoryError, self).__init__(errno.ENOTDIR, os.strerror(errno.ENOTDIR)+msg, filename)


class DirectoryIsNotEmptyError(EnvironmentError):
    """Error occur while trying to remove file-tree in non-recursive mode

          Args:
              dirname (str): Name of directory, which it tries to remove
              msg (str): Human readable string describing the exception.

          """

    def __init__(self, dirname="", msg=""):
        super(DirectoryIsNotEmptyError, self).__init__(errno.ENOTEMPTY, os.strerror(errno.ENOTEMPTY)+msg, dirname)


class NotSpecifiedItemError(Exception):
    """Error occur while trying to access bucket item by not unique data

          Args:
              item_data (str): Data by which trying access
              msg (str): Human readable string describing the exception.

          """

    def __init__(self, item_data="", msg=""):
        super (NotSpecifiedItemError,
               self).__init__("Can't specify item. There are several items with name {}".format(item_data)+msg)