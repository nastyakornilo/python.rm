import unittest
import datetime
import shutil
import os
from smart_remover.utility.policy import Policy
from smart_remover.utility.policy import AutocleanType
from smart_remover.lib.smart_bucket import SmartBucket
from smart_remover.lib.bucket_item import BucketItem
import mock

WORK_DIR = os.path.join(os.getcwd(), "BucketTests")

#ITEMS_COUNT = 100
#TOTAL_ITEMS_SIZE = 500
MAX_BUCKET_SIZE_IN_BYTES = 1000
BUCKET_DIR = os.path.join(WORK_DIR, "Bucket")
TRASH_DIR_NAME = "TrashDir"
INFOFILE_NAME = "Infofile"

TEST_FILE = os.path.join(WORK_DIR, "TestFile")

def create_file_with_size(file_path, size):
    with open(file_path, 'w') as bigfile:
        bigfile.seek(size-1)
        bigfile.write('0')

def create_test_bucket_items():
    # bucket items with not important params
    bucket_items = [BucketItem('name', 'path', 8, deletion_date=datetime.datetime(2000, 1, 1), index=0),
                    BucketItem('name', 'path', 8, deletion_date=datetime.datetime(2017, 1, 1), index=1)
                    ]
    return bucket_items


class PolicyTestCase(unittest.TestCase):
    def setUp(self):
        self.tested_policy = Policy()
        os.makedirs(WORK_DIR)

    def tearDown(self):
        shutil.rmtree(WORK_DIR)

    def test_no_autoclean(self):
        self.tested_policy.autoclean_type = AutocleanType.no_autoclean
        bucket = mock.NonCallableMock()
        method_for_autoclean_wrap = mock.Mock()
        bucket.attach_mock(method_for_autoclean_wrap,'method_for_autoclean_wrap')

        autoclean_wrapped_method = self.tested_policy.autoclean(bucket,bucket.method_for_autoclean_wrap)
        # bucket.method_for_autoclean_wrap()
        self.assertEqual(method_for_autoclean_wrap, autoclean_wrapped_method)

        # autoclean_wrapped_method.assert_called_once()

    def test_autoclean_overflow(self):
        self.tested_policy.autoclean_type = AutocleanType.overflow
        bucket = mock.NonCallableMock()
        method_for_autoclean_wrap = mock.Mock()
        bucket.attach_mock(method_for_autoclean_wrap, 'method_for_autoclean_wrap')

        autoclean_wrapped_method = self.tested_policy.autoclean(bucket,bucket.method_for_autoclean_wrap)
        autoclean_wrapped_method(bucket)
        bucket.method_for_autoclean_wrap.assert_called_once()
        # #self.assertEqual(method_for_autoclean_wrap, autoclean_wrapped_method)

    def test_autoclean_overflow_with_raised_not_enough_free_space_exception(self):
        self.tested_policy.autoclean_type = AutocleanType.overflow
        bucket = SmartBucket(BUCKET_DIR,
                             TRASH_DIR_NAME,
                             INFOFILE_NAME,
                             MAX_BUCKET_SIZE_IN_BYTES,
                             policy=self.tested_policy
                             )

        bucket._used_space_size = MAX_BUCKET_SIZE_IN_BYTES
        create_file_with_size(TEST_FILE, 1)
        bucket.remove(TEST_FILE)

    def test_autoclean_raise_type_error(self):
        self.tested_policy.earliest_deletion_time = None

        self.tested_policy.autoclean_type = AutocleanType.old_files
        self.assertRaises(TypeError, self.tested_policy.autoclean, mock.Mock())

        self.tested_policy.autoclean_type = AutocleanType.combined
        self.assertRaises(TypeError, self.tested_policy.autoclean, mock.Mock())

    def test_autoclean_old_files(self):
        self.tested_policy.autoclean_type = AutocleanType.old_files
        self.tested_policy.earliest_deletion_time = datetime.datetime(2005, 1, 1)
        bucket = mock.NonCallableMock()
        bucket.attach_mock(mock.Mock(),'method')


        bucket_items = create_test_bucket_items()
        old_item = bucket_items[0]

        bucket.attach_mock(mock.Mock(), 'clear_by_name_in_bucket')
        bucket.items = bucket_items

        bucket.method = self.tested_policy.autoclean(bucket,bucket.method)
        bucket.method()
        bucket.clear_by_name_in_bucket.assert_called_once_with(old_item.name_in_bucket)

    @mock.patch('__builtin__.raw_input', lambda x: 0)
    def test_specify_item(self):
        test_items = create_test_bucket_items()
        chosen_item = self.tested_policy.specify_item(test_items)
        self.assertEqual(chosen_item.index, 0)

    @mock.patch('__builtin__.raw_input', lambda x: -1)
    def test_specify_item_choose_none(self):
        test_items = create_test_bucket_items()
        chosen_item = self.tested_policy.specify_item(test_items)
        self.assertTrue(chosen_item is None)

if __name__ == '__main__':
    unittest.main()
