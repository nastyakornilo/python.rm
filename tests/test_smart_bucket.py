#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
import re
import shutil
import unittest
import os
from contextlib import contextmanager
from StringIO import StringIO
import sys
import mock
from mock import MagicMock
from mock import Mock
from smart_remover.lib.smart_bucket import SmartBucket
from smart_remover.lib.abstract_policy import AbstractPolicy
from smart_remover.lib.smart_bucket import remove_interactive, force, silent
from smart_remover.lib.bucket_item import BucketItem
from smart_remover.lib.bucket_infomanager import BucketInfoManager
from smart_remover.lib import bucket_errors
# sys.path.append(os.path.realpath("shared/"))
# sys.path.append(os.path.realpath("tests/"))

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

WORK_DIR = os.path.join(os.getcwd(), "BucketTests")

#ITEMS_COUNT = 100
#TOTAL_ITEMS_SIZE = 500
MAX_BUCKET_SIZE_IN_BYTES = 1000
BUCKET_DIR = os.path.join(WORK_DIR, "Bucket")
TRASH_DIR_NAME = "TrashDir"
INFOFILE_NAME = "Infofile"

TEST_FILE = os.path.join(WORK_DIR, "TestFile")

TEST_DIR = os.path.join(WORK_DIR, "TestDir")
EMPTY_SUB_DIR = os.path.join(TEST_DIR, "EmptySubDir")

SUB_FILE = os.path.join(TEST_DIR, "SubFile")

SUB_DIR = os.path.join(TEST_DIR, "SubDir")
SUB_SUB_DIR = os.path.join(SUB_DIR, "SubSubDir")
SUB_SUB_FILE = os.path.join(SUB_DIR, "SubSubFile")

DIR_WITH_CYCLE = os.path.join(WORK_DIR, "SubDirWithCycle")
SUB_SYMLINK = os.path.join(TEST_DIR, "SubSymLink")


def create_test_file_tree():
    os.makedirs(DIR_WITH_CYCLE)
    os.makedirs(SUB_SUB_DIR)
    os.makedirs(EMPTY_SUB_DIR)
    with open(TEST_FILE, 'w'):
        pass
    with open(SUB_FILE, 'w'):
        pass
    with open(SUB_SUB_FILE, 'w'):
        pass

    os.symlink(DIR_WITH_CYCLE, SUB_SYMLINK)


def create_file_with_size(file_path, size):
    with open(file_path, 'w') as bigfile:
        bigfile.seek(size-1)
        bigfile.write('0')


class BucketTestCase(unittest.TestCase):
    def setUp(self):
        create_test_file_tree()
        self.tested_bucket = SmartBucket(BUCKET_DIR, TRASH_DIR_NAME, INFOFILE_NAME, MAX_BUCKET_SIZE_IN_BYTES)

    def tearDown(self):
        shutil.rmtree(WORK_DIR)

    @mock.patch('os.path.exists', lambda x: False)
    def test_remove_raise_path_existence_error(self):
        self.assertRaises(bucket_errors.PathExistenceError, self.tested_bucket.remove,'DoesNotExistingPath')

    def test_remove_raise_path_belong_bucket_error(self):
        self.assertRaises(bucket_errors.PathBelongBucketError, self.tested_bucket.remove, BUCKET_DIR)
        self.assertRaises(bucket_errors.PathBelongBucketError,
                          self.tested_bucket.remove,
                          os.path.join(BUCKET_DIR,TRASH_DIR_NAME)
                          )
        self.assertRaises(bucket_errors.PathBelongBucketError,
                          self.tested_bucket.remove,
                          os.path.join(BUCKET_DIR, INFOFILE_NAME)
                          )

    @mock.patch('shutil.move', mock.Mock())
    def test_remove_raise_removing_bucket_parent_directory_error(self):
        self.assertRaises(bucket_errors.RemovingBucketParentDirectoryError,
                          self.tested_bucket.remove,
                          os.environ['HOME']
                          )
        self.assertRaises(bucket_errors.RemovingBucketParentDirectoryError, self.tested_bucket.remove, '/')
        self.assertRaises(bucket_errors.RemovingBucketParentDirectoryError, self.tested_bucket.remove, WORK_DIR)

    def test_remove_raise_path_is_directory_error(self):
        self.tested_bucket._directory_mode = False
        self.tested_bucket._recursive_mode = False
        self.assertRaises(bucket_errors.PathIsDirectoryError, self.tested_bucket.remove, TEST_DIR)

    def test_remove_raise_directory_is_not_empty_error(self):
        self.tested_bucket._recursive_mode = False
        self.tested_bucket._directory_mode = True
        self.assertRaises(bucket_errors.DirectoryIsNotEmptyError, self.tested_bucket.remove, TEST_DIR)

    def test_remove_raise_pesmart_rmission_error(self):
        no_write_permission_mode = 555
        os.chmod(TEST_FILE, no_write_permission_mode)
        self.assertRaises(bucket_errors.PermissionError, self.tested_bucket.remove, TEST_FILE)

    #@mock.patch('smart_remover.smart_lib.smart_bucket.SmartBucket._count_size', lambda x, y: 150)
    def test_remove_raise_not_enough_free_space_exception(self):
        self.tested_bucket._used_space_size = MAX_BUCKET_SIZE_IN_BYTES
        not_empty_file = os.path.join(WORK_DIR, 'NotEmptyFile')
        create_file_with_size(not_empty_file, size=1)

        self.assertRaises(bucket_errors.NotEnoughFreeSpaceException, self.tested_bucket.remove, not_empty_file)

    #    @mock.patch('smart_remover.smart_lib.smart_bucket.SmartBucket._count_size', lambda x, y: 0)
    @mock.patch('smart_remover.lib.smart_bucket.SmartBucket._get_name_for_storing_item', lambda x:'nameInBucket')
    def test_remove_file(self):
        expected_object_path = os.path.join(BUCKET_DIR,TRASH_DIR_NAME, 'nameInBucket')
   #       func_remove_listener = Mock()
        self.tested_bucket._dry_run = False
        self.tested_bucket.remove(TEST_FILE)
        self.assertFalse(os.path.exists(TEST_FILE))
        self.assertTrue(os.path.exists(expected_object_path))
#        func_remove_listener.assert_called_once()

    @mock.patch('smart_remover.lib.smart_bucket.SmartBucket._get_name_for_storing_item', lambda x: 'nameInBucket')
    def test_remove_empty_dir(self):
        self.tested_bucket._directory_mode = True
        expected_object_path = os.path.join(BUCKET_DIR, TRASH_DIR_NAME, 'nameInBucket')
        #       func_remove_listener = Mock()
        self.tested_bucket._dry_run = False
        self.tested_bucket.remove(EMPTY_SUB_DIR)
        self.assertFalse(os.path.isdir(EMPTY_SUB_DIR))
        self.assertTrue(os.path.exists(expected_object_path))

    @mock.patch('smart_remover.lib.smart_bucket.SmartBucket._get_name_for_storing_item', lambda x: 'nameInBucket')
    def test_remove_dir(self):
        self.tested_bucket._recursive_mode = True
        expected_object_path = os.path.join(BUCKET_DIR, TRASH_DIR_NAME, 'nameInBucket')
        #       func_remove_listener = Mock()
        self.tested_bucket._dry_run = False
        self.tested_bucket.remove(TEST_DIR)
        self.assertFalse(os.path.isdir(TEST_DIR))
        self.assertTrue(os.path.exists(expected_object_path))

    #        func_remove_listener.assert_called_once()

    def test_remove_dry_run(self):
        expected_object_path = os.path.join(BUCKET_DIR, TRASH_DIR_NAME, 'nameInBucket')
        self.tested_bucket._dry_run = True
        self.tested_bucket.remove(TEST_FILE)
        self.assertTrue(os.path.exists(TEST_FILE))
        self.assertFalse(os.path.exists(expected_object_path))

    @mock.patch('smart_remover.lib.smart_bucket.SmartBucket._get_name_for_storing_item', lambda x: 'nameInBucket')
    def test_remove_dir_with_cycle(self):
        self.tested_bucket._recursive_mode = True
        expected_object_path = os.path.join(BUCKET_DIR, TRASH_DIR_NAME, 'nameInBucket')
        #       func_remove_listener = Mock()
        self.tested_bucket._dry_run = False
        self.tested_bucket.remove(DIR_WITH_CYCLE)
        self.assertFalse(os.path.isdir(DIR_WITH_CYCLE))
        self.assertTrue(os.path.exists(expected_object_path))

    def test_remove_tree_by_regex(self):
        remove_with_substring_dir_expected_result = not os.path.exists(EMPTY_SUB_DIR) and not os.path.exists(
            SUB_SUB_DIR)
        #remove_with_substring_file_expected_result = ['subsub_file', 'sub_file']
        #remove_with_substring_sub_expected_result = ['subsub_file', 'subsub_dir', 'sub_file']
        self.tested_bucket.remove_tree_by_regex(TEST_DIR, 'Dir')
        self.assertFalse(os.path.isdir(EMPTY_SUB_DIR) or os.path.exists(SUB_SUB_DIR))
       # self.assertEqual(self.tested_bucket.remove_tree_by_regex(test_tree, 'file'), remove_with_substring_file_expected_result)
       # self.assertEqual(self.tested_bucket.remove_tree_by_regex(test_tree, 'sub'), remove_with_substring_sub_expected_result)

    def test_restore_by_basename(self):
        self.tested_bucket.querist = Mock()
        self.tested_bucket.querist.want_restore_by_basename = lambda basename: True

        self.tested_bucket.policy = Mock()
        new_name = "New name"
        parent_dir = os.path.dirname(TEST_FILE)
        restorable_file_expected_path = os.path.join(parent_dir, new_name)
        self.tested_bucket.policy.solve_path_collision = lambda path: restorable_file_expected_path

        self.tested_bucket.remove(TEST_FILE)
        self.assertFalse(os.path.exists(TEST_FILE))

        self.tested_bucket._dry_run = True
        self.tested_bucket.restore_by_basename(os.path.basename(TEST_FILE))
        self.assertFalse(os.path.exists(restorable_file_expected_path))

        self.tested_bucket._dry_run = False
        self.tested_bucket.restore_by_basename(os.path.basename(TEST_FILE))
        self.assertTrue(os.path.exists(restorable_file_expected_path))

    def test_restore_by_basename_raise_not_specified_item_error(self):
        self.tested_bucket.policy = Mock()
        self.tested_bucket.policy.specify_item = lambda all_items_with_name: BucketItem('Does not existing item',
                                                                                            'path', 0)
        self.tested_bucket.remove(TEST_FILE)
        with open(TEST_FILE,'w'):
            pass
        self.tested_bucket.remove(TEST_FILE)

        self.assertRaises(bucket_errors.NotSpecifiedItemError,
                          self.tested_bucket.restore_by_basename,
                          os.path.basename(TEST_FILE))

    def test_restore_by_basename_solve_not_specified_item_problem(self):
        self.tested_bucket.policy = Mock()
        self.tested_bucket.policy.specify_item = lambda all_items_with_name: all_items_with_name[0]
        self.tested_bucket.policy.solve_path_collision = lambda path: TEST_FILE

        self.tested_bucket.remove(TEST_FILE)
        with open(TEST_FILE, 'w'):
            pass

        self.tested_bucket.remove(TEST_FILE)
        self.assertFalse(os.path.exists(TEST_FILE))

        self.tested_bucket._dry_run = True
        self.tested_bucket.restore_by_basename(os.path.basename(TEST_FILE))
        self.assertFalse(os.path.exists(TEST_FILE))

        self.tested_bucket._dry_run = False
        self.tested_bucket.restore_by_basename(os.path.basename(TEST_FILE))
        self.assertTrue(os.path.exists(TEST_FILE))

    def test_restore_by_index(self):
        new_name = "New name"
        self.tested_bucket.policy = Mock()
        parent_dir = os.path.dirname(TEST_FILE)
        restorable_file_expected_path = os.path.join(parent_dir, new_name)
        self.tested_bucket.policy.solve_path_collision = lambda path: restorable_file_expected_path

        self.tested_bucket.remove(TEST_FILE)
        self.assertFalse(os.path.exists(TEST_FILE))

        self.tested_bucket._dry_run = True
        removed_bucket_item = self.tested_bucket.restore_by_index(0)
        self.assertEqual(removed_bucket_item.index, 0)
        self.assertFalse(os.path.exists(restorable_file_expected_path))

        self.tested_bucket._dry_run = False
        removed_bucket_item = self.tested_bucket.restore_by_index(0)
        self.assertEqual(removed_bucket_item.index, 0)
        self.assertTrue(os.path.exists(restorable_file_expected_path))

    @mock.patch('smart_remover.lib.smart_bucket.SmartBucket._get_name_for_storing_item', lambda x: 'nameInBucket')
    def test_clear_by_index(self):
        self.tested_bucket.remove(TEST_FILE)
        file_path_in_bucket = os.path.join(BUCKET_DIR, TRASH_DIR_NAME, 'nameInBucket')
        self.assertFalse(os.path.exists(TEST_FILE))
        self.assertTrue(os.path.exists(file_path_in_bucket))

        self.tested_bucket._dry_run = True
        self.tested_bucket.clear_by_index(0)
        self.assertTrue(os.path.exists(file_path_in_bucket))

        self.tested_bucket._dry_run = False
        self.tested_bucket.clear_by_index(0)
        self.assertFalse(os.path.exists(file_path_in_bucket))

    @mock.patch('smart_remover.lib.smart_bucket.SmartBucket._get_name_for_storing_item', lambda x: 'nameInBucket')
    def test_clear_by_basename(self):
        self.tested_bucket.remove(TEST_FILE)
        file_path_in_bucket = os.path.join(BUCKET_DIR, TRASH_DIR_NAME, 'nameInBucket')
        self.assertFalse(os.path.exists(TEST_FILE))
        self.assertTrue(os.path.exists(file_path_in_bucket))

        self.tested_bucket._dry_run = True
        self.tested_bucket.clear_by_basename(os.path.basename(TEST_FILE))
        self.assertTrue(os.path.exists(file_path_in_bucket))

        self.tested_bucket._dry_run = False
        self.tested_bucket.clear_by_basename(os.path.basename(TEST_FILE))
        self.assertFalse(os.path.exists(file_path_in_bucket))

    def test_enable_silent(self):
        self.tested_bucket._silent = False
        self.tested_bucket.enable_modes(silent=True)
        self.assertTrue(self.tested_bucket._silent)
        self.assertFalse(self.tested_bucket._interactive)

    def test_enable_force(self):
        self.tested_bucket._force = False
        self.tested_bucket.enable_modes(force=True)
        self.assertTrue(self.tested_bucket._force)
        self.assertFalse(self.tested_bucket._interactive)

    def test_enable_interactive(self):
        self.tested_bucket._interactive = False
        self.tested_bucket.enable_modes(interactive=True)
        self.assertTrue(self.tested_bucket._interactive)
        self.assertFalse(self.tested_bucket._silent)
        self.assertFalse(self.tested_bucket._force)

    def test_enable_recursive(self):
        self.tested_bucket._recursive_mode = False
        self.tested_bucket.enable_modes(recursive=True)
        self.assertTrue(self.tested_bucket._recursive_mode)

    def test_enable_directory_mode(self):
        self.tested_bucket._directory_mode = False
        self.tested_bucket.enable_modes(directory=True)
        self.assertTrue(self.tested_bucket._directory_mode)

    def test_enable_verbose(self):
        self.tested_bucket.result_writer = Mock()
        self.tested_bucket._verbose = False
        self.tested_bucket.enable_modes(verbose=True)
        self.assertTrue(self.tested_bucket._verbose)
        self.assertFalse(self.tested_bucket._silent)

    def test_enable_dry_run(self):
        self.tested_bucket._dry_run = False
        self.tested_bucket.enable_modes(dry_run=True)
        self.assertTrue(self.tested_bucket._dry_run)

    def test_force_none_result_instead_of_error(self):
        self.tested_bucket._force = True

        def method(self_param):
            raise bucket_errors.PathExistenceError
        mock_force_method = force(method)
        self.assertTrue(mock_force_method(self.tested_bucket) is None)

    def test_force_method_result(self):
        self.tested_bucket._force = True

        def method(self_param):
            return "result"
        mock_force_method = force(method)
        self.assertTrue(mock_force_method(self.tested_bucket) == "result")

    def test_silent_result_with_exception_raised(self):
        self.tested_bucket._silent = True

        def method(self_param):
            raise Exception

        silent_method=silent(method)
        self.assertEqual(silent_method(self.tested_bucket),1)

    def test_silent_result_no_exception_raised(self):
        self.tested_bucket._silent = True

        def method(self_param):
            pass

        silent_method = silent(method)
        self.assertEqual(silent_method(self.tested_bucket), 0)

if __name__ == '__main__':
    unittest.main()
