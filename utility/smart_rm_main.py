#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""This module contain entry point to the smart_rm utility"""

import logging
from smart_remover.utility.config import get_arguments, configure_logger
from smart_remover.utility.policy import Policy
from smart_remover.utility.config import get_dict_config_from_file, get_dict_config, merge_dict_configs
from smart_remover.utility.console_interaction import ConsoleQuerist, ConsoleResultWriter
from smart_remover.utility.action_manager import ActionManager
from smart_remover.lib.smart_bucket import SmartBucket
import sys


def main():
    try:
        sys.tracebacklimit = 0
        console_config = get_dict_config(get_arguments())
        file_config = get_dict_config_from_file(console_config['config'])

        config = merge_dict_configs(console_config, file_config)
        configure_logger(**config['logger'])

        bucket = SmartBucket(querist=ConsoleQuerist(),
                             result_writer=ConsoleResultWriter(),
                             policy=Policy(**config['policy']),
                             **config['bucket']
                             )

        bucket.enable_modes(**config['mode'])

        action_manager = ActionManager(bucket, config['action_type'])
        action_manager.action(files=config['files'],**config['action_params'])

    except KeyboardInterrupt:
        logging.warning('Smart_rm was stopped by keyboard interrupt')

if __name__ == '__main__':
    main()

