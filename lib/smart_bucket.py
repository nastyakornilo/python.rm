# -*- coding: utf-8 -*-
"""
This module contain SmartBucket class for removing, restoring, clean files
and decorators for interactive, silent, force modes

"""
import itertools
import multiprocessing
import errno
import os
import re
import shutil
import sys
import uuid
import logging
import contextlib
from smart_remover.lib import bucket_errors as err
from smart_remover.lib.bucket_item import BucketItem
from smart_remover.lib.bucket_infomanager import BucketInfoManager
from observer import Event


MAX_BUCKET_SIZE_IN_BYTES = 1024*1024


def remove_interactive(method):
    """This decorator makes remove method interactive (ask user for executing permission)

    Args:
        method: Method for decorating.

    Returns:
        bool: Return interactive method (ask user for executing method permission)

    """
    def inner(self, path_to_removable_object):
        if self._interactive:
            chosen_files = self._choose_files_for_remove(path_to_removable_object)
            for chosen_file in chosen_files:
                return method(self, chosen_file)
        else:
            return method(self, path_to_removable_object)
    return inner


def force(method):
    """This decorator makes method force (ignore PathExistenceError)

    Args:
        method: Method for decorating.

    Returns:
        bool: Return method, which ignore PathExistenceError while executing

    """

    def inner(self, *args, **kwargs):
        if self._force:
            try:
                return method(self,*args, **kwargs)
            except err.PathExistenceError:
                pass
        else:
            return method(self, *args, **kwargs)
    return inner


def silent(method):
    """This decorator makes method silent (ignore all errors)

    Args:
        method: Method for decorating.

    Returns:
        bool: Return method, which ignore all errors while executing

    """
    def inner(self, *args, **kwargs):
        if self._silent:
            self.logger.disabled = True
            old_stdout = sys.stdout

            sys.stdout = os.devnull
            try:
                method(self,*args, **kwargs)
                return 0
            except Exception:
                return 1
            finally:
                sys.stdout = old_stdout
        else:
            return method(self, *args, **kwargs)
    return inner

class SmartBucket(object):
    """Smart bucket for removing, restoring, clearing fils and directories
with recursive, directory, verbose, silent, force, dry-run modes support

    Args:
        bucket_dir (str): Path to bucket directory
        trashfiles_dir_name (str): Name of directory with removed files
        infofile (str): Name of file for storing data about removed files
        max_bucket_size_in_bytes (int): total size of removed files, than bucket can accommodate
        policy: object for solving problems while bucket work
        result_writer: object for writing done actions
        querist: object for asking user for confirmation
    Attributes:
        bucket_dir (str): Path to bucket directory
        trashfiles_dir_name (str): Name of directory with removed files
        infofile (str): Name of file for storing data about removed files
        max_bucket_size_in_bytes (int): total size of removed files, than bucket can accommodate
        policy: object for solving problems while bucket work
        result_writer: object for writing done actions
        querist: object for asking user for confirmation
        logger: object for technical logging
    """

    def __init__(self,
                 bucket_dir,
                 trashfiles_dir_name='TrashFiles',
                 infofile='InfoFile',
                 max_bucket_size_in_bytes=MAX_BUCKET_SIZE_IN_BYTES,
                 policy=None,
                 result_writer=None,
                 querist=None
                 ):
        #self.logger = logging.getLogger('')
        self.logger = logging.getLogger('')
       # null_handler = logging.NullHandler
        #self.logger.addHandler(null_handler)
        self.logger.addHandler(logging.FileHandler(os.devnull))
        self._bucket_dir = bucket_dir
        self._deleted_files_dir = os.path.join(self._bucket_dir, trashfiles_dir_name)
        self._infofile = os.path.join(self._bucket_dir, infofile)

        self.policy = policy
        if not (self.policy is None):
            self.remove = self.policy.autoclean(self, self.remove)

        self.result_writer = result_writer
        self.querist = querist

        self.clear_event = Event()
        self.clean_event = Event()
        self.remove_event = Event()
        self.restore_event = Event()

        self._removable_obj_total_size = 0
        self._done_size = 0

        self._total_space_size = max_bucket_size_in_bytes
        #self._new_item_size = 0

        self._dry_run = False
        self._interactive = False
        self._verbose = False
        self._silent = False
        self._force = False
        self._recursive_mode = False
        self._directory_mode = False

        if not os.path.exists(self._bucket_dir) and not self._dry_run:
            os.makedirs(self._bucket_dir)

        if not os.path.exists(self._deleted_files_dir) and not self._dry_run:
            os.makedirs(self._deleted_files_dir)

        self._infofile_manager = BucketInfoManager(self._infofile)
        self._removed_items_count = self._infofile_manager.get_count()

        self._used_space_size = self._infofile_manager.get_total_items_size()

       # if self._space_size < self._used_space_size:
#                raise BucketOverflowError

    @property
    def removed_items_count(self):
        """int: Return count of removed items."""
        return self._removed_items_count
    
    @property
    def space_size(self):
        """int: Return bucket space size."""
        return self._total_space_size

    @property
    def used_space_size(self):
        """str: Return used space size"""
        return self._used_space_size

    # def autoclean(self):
    #     if self.policy is not None and self.policy.autoclean_condition(self):
    #         self.clean()

    @property
    def items(self):
        return self._infofile_manager.get_all_items_in_range()

    @silent
    @force
    @remove_interactive
    def remove(self, removable_item_path):
        """Method removes file/directory

        Args:
            removable_item_path (str): Path to item for removing
            
        Returns:
            BucketItem: if removing successeful - return new bucket item 
                storing removed item info, None otherwise.

        Raises:
            PathExistenceError: if removable_item_path does not exists
            PathBelongBucketError: if removable_item_path is inner bucket file 
                (removable_item_path startswith self._bucket_dir)
            RemovingBucketParentDirectoryError: if removable_item_path is 
                a directory - parent(ancestor) for self._bucket_dir
            PathIsDirectoryError: if removable_item_path is directory, 
                but bucket works in none-directory mode
            DirectoryIsNotEmptyError: if removable_item_path is not empty directory,
                but bucket works in non-recursive mode
            PermissionError: if user does not have writing writes for removable_item_path 
            NotEnoughFreeSpaceException: if size of removable_item_path is more than
                bucket free space 
            
        """
        if not os.path.exists(removable_item_path):
            self.logger.error("Path existence error: {}".format(removable_item_path))
            raise err.PathExistenceError(removable_item_path)

        if removable_item_path.startswith(self._bucket_dir):
            self.logger.error("Path belong to bucket {}".format(removable_item_path))
            raise err.PathBelongBucketError(removable_item_path)

        if self._deleted_files_dir.startswith(removable_item_path):
            self.logger.error("Error while trying move {} inside {}".format(removable_item_path,self._deleted_files_dir))
            raise err.RemovingBucketParentDirectoryError(removable_item_path)

        if os.path.isdir(removable_item_path):
            if not (self._directory_mode or self._recursive_mode):
                self.logger.error("Given path {} is directory".format(removable_item_path))
                raise err.PathIsDirectoryError(removable_item_path)
            if os.listdir(removable_item_path) and not self._recursive_mode:
                self.logger.error("Directory {} is not empty".format(removable_item_path))
                raise err.DirectoryIsNotEmptyError(removable_item_path)

        if not os.access(removable_item_path, os.W_OK):
            self.logger.error(("Can't remove {}" + os.strerror(errno.EACCES)).format(removable_item_path))
            raise err.PermissionError(removable_item_path)

        #self.autoclean()
        self._removable_obj_total_size = self._count_size(removable_item_path)
        #
        # if self._removable_obj_total_size > self._space_size:
        #     self.logger.error("Object size more than max bucket size")
        #     raise err.ObjectSizeExceedeBucketMaxSizeError(removable_item_path)

        used_size_after_remove = self._used_space_size + self._removable_obj_total_size

        if used_size_after_remove > self._total_space_size:
            self.logger.error(("Can't remove {}" + os.strerror(errno.ENOMEM)).format(removable_item_path))
            raise err.NotEnoughFreeSpaceException(removable_item_path)

        new_item = self._register_bucket_item(removable_item_path)
        object_path_in_bucket = os.path.join(self._deleted_files_dir, new_item.name_in_bucket)
        self._total_space_size = used_size_after_remove

        if not self._dry_run:
            shutil.move(removable_item_path, object_path_in_bucket)
            #self._move(removable_item_path, object_path_in_bucket)

        self._removed_items_count += 1
        self.remove_event.trigger(new_item)
        self.logger.info("Removed:{}".format(removable_item_path))
        return new_item
        # self._unregister_bucket_item(os.path.basename(file_path_in_bucket))
        #          raise

    @silent
    @force
    def remove_tree_by_regex(self, removable_dir_path, regex):
        """Method removes files and directories in tree by regex

        Args:
            removable_dir_path (str): Path to directory
            regex: regular expression for searching files for remove

        Returns:
            list: if removing successeful - return list of new bucket items,
                None otherwise.

        """


        pattern = re.compile(regex)
        pool_size = multiprocessing.cpu_count() * 2
        pool = multiprocessing.Pool(processes=pool_size)
        dir_content = [os.path.join(removable_dir_path, name) for name in os.listdir(removable_dir_path)]
        matchable_paths = pool.map(find_matchable_files_unpacker_wrapper,
                                   itertools.izip(dir_content, itertools.repeat(pattern))
                                   )
        matchable_paths=sum(matchable_paths, [])
        pool.close()
        pool.join()

        # matchable_paths = find_matchable_files(removable_dir_path)
        cash_recursive_mode = self._recursive_mode
        self._recursive_mode = True

        result = []


        for path in matchable_paths:
            result.append(self.remove(path))

        self._recursive_mode = cash_recursive_mode
        self.logger.info("Recursively removed files by regex from {}".format(removable_dir_path))
        return result

        #

    # for root, dirs, files in os.walk(removable_item_path):
    #     for file in filter(lambda x: re.match(regex, x), files):
    #         matchable_paths.append(os.path.join(root, file))
    #     for directory in filter(lambda x: re.match(regex, x), dirs):
    #         matchable_paths.append(os.path.join(root, directory))



    @silent
    @force
    def restore_by_basename(self, basename):
        """Method restores item by basename

        Args:
           basename (str): basename of file/directory for restore

        Returns:
           BucketItem: if restoring successeful - return restored bucket item
               storing removed item info, None otherwise.

        """
        item_with_given_name = self._get_item_by_basename(basename)
        if not self._interactive or (self._interactive and self.querist.want_restore_by_basename(basename)):
            self._restore_item(item_with_given_name)
            self.logger.info("Restored item by basename {}".format(basename))
        return item_with_given_name

    #@silent
    #@force
    def _get_item_by_basename(self, basename):
        try:
            return self._infofile_manager.get_item_by_basename(basename)
        except err.NotSpecifiedItemError:
            self.logger.warning("Not specified item for clearing")
            all_items_with_name = self._infofile_manager.get_all_items_by_basename(basename)
            chosen_item = self.policy.specify_item(all_items_with_name)
            if not (chosen_item in all_items_with_name):
                self.logger.error("Not specified item for clearing")
                raise
            return chosen_item

    @silent
    @force
    def restore_by_index(self, index):
        """Method restores item by index

        Args:
           basename (str): basename of file/directory for restore

        Returns:
           BucketItem: if restoring was done - return restored bucket item,
               None otherwise.

        """
        restoring_bucket_item = self._infofile_manager.get_item_by_index(index)
        if not self._interactive or (self._interactive and self.querist.want_restore_by_index(index)):
            self._restore_item(restoring_bucket_item)
            self.logger.info("Item {} restored by index".format(index))
        return restoring_bucket_item

    def _restore_item(self, restoring_item):
        item_path_in_bucket = os.path.join(self._deleted_files_dir, restoring_item.name_in_bucket)
        target = self.policy.solve_path_collision(restoring_item.old_path)
        if not self._dry_run:
            shutil.move(item_path_in_bucket, target)
            #self._move(item_path_in_bucket, target)
            self._unregister_bucket_item(restoring_item.name_in_bucket)
            self._removed_items_count -= 1
            self.restore_event.trigger(restoring_item)

    @silent
    @force
    def clean(self):
        """Clean bucket

        Returns:
           None
        """
        if (not self._interactive or (self._interactive and self.querist.want_clean)) and not self._dry_run:
            for root, dirs, files in os.walk(self._deleted_files_dir, topdown=False):
                for name in files:
                    os.remove(os.path.join(root, name))
                for name in dirs:
                    os.rmdir(os.path.join(root, name))
            #shutil.rmtree(self._deleted_files_dir)
            #os.makedirs(self._deleted_files_dir)
            self._infofile_manager.clear_content()
            self._removed_items_count = 0
            self._used_space_size = 0
            self.clean_event.trigger()

            self.logger.info("Bucket cleaned")

    @silent
    @force
    def clear_by_index(self, index):
        """Clear item by index function

        Args:
           index (int): index of item to remove

        Returns:
           BucketItem: if clearing was done - return cleared bucket item,
               None otherwise.

        """
        clearable_bucket_item = self._infofile_manager.get_item_by_index(index)
        if not self._interactive or (self._interactive and self.querist.want_clear_by_index(index)):
            self.clear_by_name_in_bucket(clearable_bucket_item.name_in_bucket)
            self._removed_items_count -= 1
            self.clear_event.trigger(clearable_bucket_item)
            self.logger.info("Bucket clear item by index")
        return clearable_bucket_item

    @silent
    @force
    def clear_by_basename(self, basename):
        """Method clears item by basename

        Args:
           basename (str): basename of item to remove

        Returns:
           BucketItem: if clearing was done - return cleared bucket item,
               None otherwise.

        """

        clearable_bucket_item = self._infofile_manager.get_item_by_basename(basename)
        if not self._interactive or (self._interactive and self.querist.want_clear_by_basename(basename)):
            self.clear_by_name_in_bucket(clearable_bucket_item.name_in_bucket)
            self.clear_event.trigger(clearable_bucket_item)
            self._removed_items_count -= 1
            self.logger.info("Bucket clear item by basename name: {}".format(basename))
        return clearable_bucket_item

    @silent
    @force
    def clear_by_name_in_bucket(self, name_in_bucket):
        removable_item_path = os.path.join(self._deleted_files_dir, name_in_bucket)
        if not self._dry_run:
            self._unregister_bucket_item(name_in_bucket)

            if os.path.isdir(removable_item_path):
                shutil.rmtree(removable_item_path)
            else:
                os.remove(removable_item_path)
        self.logger.info("Bucket clear item by name in bucket: {}".format(name_in_bucket))

    @silent
    @force
    def show_content(self, start_index, last_index):
        """Show bucket content

        Args:
           start_index (int): start index in range to show
           last_index (int): last index in range to show

        Returns:
           list: return list of bucket items

        """

        bucket_range_content = self._infofile_manager.get_all_items_in_range(start_index, last_index)
        self.logger.info("Bucket shows content. Content: {}".format(bucket_range_content))
        return bucket_range_content

    def _register_bucket_item(self, removable_item_path):
        item_name_in_bucket = self._get_name_for_storing_item()
        bucket_item = BucketItem(item_name_in_bucket,
                                 removable_item_path,
                                 self._removable_obj_total_size
                                 )
        if not self._dry_run:
            self._infofile_manager.write_item(bucket_item, bucket_item.name_in_bucket)
        return bucket_item

    def _get_name_for_storing_item(self):
        return str(uuid.uuid4())

    def _unregister_bucket_item(self, name_in_bucket):
        self._infofile_manager.remove_item(name_in_bucket)
    #
    # def _move(self, full_path, target):
    #     moving_item = self._creating_moving_bucket_item(full_path)
    #     if (not os.path.isdir):
    #         shutil.move(full_path, target)
    #         self.remove_event.trigger(moving_item)
    #
    #     else:
    #         os.mkdir(target)
    #
    #         for name in os.listdir(full_path):
    #             path = os.path.join(full_path, name)
    #             target_for_name = os.path.join(target, name)
    #             self._move(path, target_for_name)
    #
    #         os.rmdir(full_path)
    #     self.remove_event.trigger(moving_item)

    def _choose_files_for_remove(self, curpath):
        chosen_paths = set()

        if os.path.isfile(curpath):
            if self.querist.want_del_file(curpath):
                chosen_paths.add(curpath)
        else:

            dir_content_paths = [os.path.join(curpath, name) for name in os.listdir(curpath)]

            if dir_content_paths != [] and self.querist.enter_dir(curpath):

                for full_path in dir_content_paths:
                    chosen_paths.update(self._choose_files_for_remove(full_path))

            if chosen_paths.issuperset(set(dir_content_paths)):
                if self.querist.want_del_empty_dir(curpath):
                    chosen_paths.add(curpath)
                    chosen_paths.difference_update(dir_content_paths)

        return chosen_paths

    def _enable_verbose(self):
        self._silent = False
        self._verbose = True
        self.remove_event.on(self.result_writer.write_removed)
        self.clean_event.on(self.result_writer.write_clean)
        self.remove_event.on(self._count_size_change)
        self.restore_event.on(self.result_writer.write_restored)
        self.clear_event.on(self.result_writer.write_clear)

    def _count_size_change(self, removed_item):
        self._done_size += removed_item.size_in_bytes
        self._used_space_size += removed_item.size_in_bytes
        #self.progress_writer.write(self._done_size, self._removable_obj_total_size)

    @silent
    @force
    def enable_modes(self, silent=False, force=False, interactive=False,
                     recursive=False, directory=False, verbose=False, dry_run=False):
        """Method enables bucket modes

            Args:
               silent (bool): enable silent mode
               force (bool): enable force mode
               interactive (bool): enable interactive mode
               recursive (bool): enable recursive mode
               directory (bool): enable directory mode
               verbose (bool): enable verbose mode
               dry_run (bool): enable dry_run mode

            Returns:
               None: None

            """

        if silent:
            self._silent = True
            self._interactive = False
        if force:
            self._force = True
            self.interactive = False
        if interactive:
            self._silent = False
            self._force = False
            self._interactive = True
        if verbose:
            self._enable_verbose()
        if recursive:
            self._recursive_mode=True
        if directory:
            self._directory_mode = True
        if dry_run:
            self._dry_run=True
           # self._enable_verbose()

    def _count_size(self, removable_item_path):
        if os.path.isfile(removable_item_path) or os.path.islink(removable_item_path):
            return os.path.getsize(removable_item_path)
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(removable_item_path):
            for filename in filenames:
                filepath = os.path.join(dirpath, filename)
                total_size += os.path.getsize(filepath)
        return total_size


def _find_matchable_files(dir_path, pattern):

    matchable_paths = []
    if os.path.isdir(dir_path):
        for name in os.listdir(dir_path):
            path = os.path.join(dir_path, name)
            if os.path.isdir(path):
                recursion_result = _find_matchable_files(path, pattern)
                if recursion_result is not None:
                    matchable_paths.extend(recursion_result)

            elif re.search(pattern, name):
                matchable_paths.append(path)

    if re.search(pattern, os.path.basename(dir_path)):
        if matchable_paths == []:
            matchable_paths.append(dir_path)
        else:
            matchable_paths = []
            matchable_paths.append(dir_path)
    return matchable_paths


def find_matchable_files_unpacker_wrapper(args):
    return _find_matchable_files(*args)