#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
import unittest
import os
from smart_remover.lib.bucket_infomanager import BucketInfoManager
from smart_remover.lib import bucket_errors as err
from smart_remover.lib.bucket_item import BucketItem
INFO_FILE = os.path.join(os.getcwd(), "InfoFile")

class BucketInfoManagerTestCase(unittest.TestCase):
    def setUp(self):
        self.tested_manager = BucketInfoManager(INFO_FILE)

    def tearDown(self):
        os.remove(INFO_FILE)

    def test_get_item_by_unique_name_raise_bucket_not_contain_item_error(self):
        self.assertRaises(err.BucketDoesNotContainItemError,
                          self.tested_manager.get_item_by_unique_name,
                          'non_existent_name'
                          )

        first_test_item = BucketItem('unique_name_in_bucket1', 'old_path', 1, index=0)
        self.tested_manager.write_item(first_test_item, first_test_item.name_in_bucket)

        self.assertRaises(err.BucketDoesNotContainItemError,self.tested_manager.get_item_by_unique_name,'name')

    def test_get_item_by_index_raise_index_error(self):
        self.assertRaises(IndexError, self.tested_manager.get_item_by_index, 0)

        first_test_item = BucketItem('unique_name_in_bucket1', 'old_path', 1, index=0)
        second_test_item = BucketItem('unique_name_in_bucket2', 'old_path', 12, index=1)
        third_test_item = BucketItem('unique_name_in_bucket3', 'old_path', 3, index=2)

        test_items_list = [first_test_item, second_test_item, third_test_item]
        for item in test_items_list:
            self.tested_manager.write_item(item, item.name_in_bucket)

        self.assertRaises(IndexError, self.tested_manager.get_item_by_index, 3)

    def test_get_item_by_index(self):
        first_test_item = BucketItem('unique_name_in_bucket1', 'old_path', 1, index=0)
        second_test_item = BucketItem('unique_name_in_bucket2', 'old_path', 12, index=1)
        third_test_item = BucketItem('unique_name_in_bucket3', 'old_path', 3, index=2)
        forth_test_item = BucketItem('unique_name_in_bucket4', 'old_path', 15, index=3)

        test_items_list = [first_test_item, second_test_item, third_test_item, forth_test_item]
        for item in test_items_list:
            self.tested_manager.write_item(item, item.name_in_bucket)

        self.assertEqual(first_test_item, self.tested_manager.get_item_by_index(0))
        self.assertEqual(third_test_item, self.tested_manager.get_item_by_index(2))
        self.assertEqual(forth_test_item, self.tested_manager.get_item_by_index(3))
        self.assertEqual(forth_test_item, self.tested_manager.get_item_by_index(-1))

    def test_remove_item(self):
        test_item = BucketItem('unique_name_in_bucket1', 'old_path', 1, index=0)
        self.tested_manager.write_item(test_item, test_item.name_in_bucket)
        self.assertEqual(len(self.tested_manager.get_all_items_in_range()), 1)

        self.tested_manager.remove_item(section_name=test_item.name_in_bucket)
        self.assertEqual(len(self.tested_manager.get_all_items_in_range()), 0)

    def test_clear_content(self):
        with open(INFO_FILE, "w") as infofile:
            infofile.write('1234567890')
        self.tested_manager.clear_content()
        self.assertTrue(os.path.getsize(INFO_FILE) == 0)

    def test_get_all_items(self):

        self.maxDiff=None

        self.assertListEqual([], self.tested_manager.get_all_items_in_range())

        first_test_item = BucketItem('unique_name_in_bucket1', 'old_path', 1, index=0)
        second_test_item = BucketItem('unique_name_in_bucket2', 'old_path', 12, index=1)
        third_test_item = BucketItem('unique_name_in_bucket3', 'old_path', 3, index=2)
        forth_test_item = BucketItem('unique_name_in_bucket4', 'old_path', 15, index=3)

        test_items_list = [first_test_item, second_test_item, third_test_item, forth_test_item]
        for item in test_items_list:
            self.tested_manager.write_item(item, item.name_in_bucket)

        self.assertListEqual(test_items_list, self.tested_manager.get_all_items_in_range())

        self.assertListEqual([first_test_item, second_test_item],
                             self.tested_manager.get_all_items_in_range(0,2))

        self.assertListEqual([first_test_item, second_test_item],
                             self.tested_manager.get_all_items_in_range(None, 2))

        self.assertListEqual([],self.tested_manager.get_all_items_in_range(0, 0))

        self.assertListEqual([],self.tested_manager.get_all_items_in_range(-1, 2))
        self.assertListEqual([], self.tested_manager.get_all_items_in_range(100, 200))
        self.assertListEqual([], self.tested_manager.get_all_items_in_range(2, 0))

        second_test_item.index = -3  # random number
        third_test_item.index = -2  # one more random number

        self.assertListEqual([second_test_item, third_test_item],
                             self.tested_manager.get_all_items_in_range(-3, -1))

        self.tested_manager = BucketInfoManager(INFO_FILE)

        second_test_item.index = 1
        third_test_item.index = 2

        self.assertListEqual(test_items_list, self.tested_manager.get_all_items_in_range())

    def test_get_count(self):
        self.tested_manager.write_item(BucketItem('unique_name_in_bucket1','old_path',1), 'unique_name_in_bucket1')
        self.tested_manager.write_item(BucketItem('unique_name_in_bucket2', 'old_path', 2), 'unique_name_in_bucket2')
        self.tested_manager.write_item(BucketItem('unique_name_in_bucket3', 'old_path', 3), 'unique_name_in_bucket3')

        self.assertEqual(self.tested_manager.get_count(), 3)

        # return len(self.get_all_items())

    def test_get_total_items_size(self):
        self.assertEqual(self.tested_manager.get_total_items_size(), 0)

        self.tested_manager.write_item(BucketItem('unique_name_in_bucket1', 'old_path', 1), 'unique_name_in_bucket1')
        self.assertEqual(self.tested_manager.get_total_items_size(), 1)

        self.tested_manager.write_item(BucketItem('unique_name_in_bucket2', 'old_path', 2), 'unique_name_in_bucket2')
        self.tested_manager.write_item(BucketItem('unique_name_in_bucket3', 'old_path', 3), 'unique_name_in_bucket3')

        self.assertEqual(self.tested_manager.get_total_items_size(), 6)

    def test_get_all_items_with_name(self):
        first_test_item = BucketItem('unique_name_in_bucket1', 'basename', 1, index=0)
        self.tested_manager.write_item(first_test_item, 'unique_name_in_bucket1')
        second_test_item = BucketItem('unique_name_in_bucket2', 'basename', 2, index=1)
        self.tested_manager.write_item(second_test_item, 'unique_name_in_bucket2')
        third_test_item = BucketItem('unique_name_in_bucket3', 'other_basename', 3, index=2)
        self.tested_manager.write_item(third_test_item, 'unique_name_in_bucket3')

        result = self.tested_manager.get_all_items_by_basename('basename')
        expected_list = [first_test_item, second_test_item]
        self.assertListEqual(expected_list, result)

        result = self.tested_manager.get_all_items_by_basename('non_existent_name')
        expected_list = []
        self.assertListEqual(expected_list, result)

    def test_get_item_by_name_raise_not_contain_such_item(self):
        self.assertRaises(err.BucketDoesNotContainItemError,
                          self.tested_manager.get_item_by_basename,
                          'Not removed path'
                          )

    def test_get_item_by_name_raise_not_specified_item_error(self):
        path = os.path.join('some_path','basename')
        other_path_with_such_basename = os.path.join('other_path', 'basename')

        self.tested_manager.write_item(BucketItem('unique_name_in_bucket1', path, 1), 'unique_name_in_bucket1')
        self.tested_manager.write_item(BucketItem('unique_name_in_bucket2', other_path_with_such_basename, 2),
                                       'unique_name_in_bucket2')

        self.assertRaises(err.NotSpecifiedItemError,self.tested_manager.get_item_by_basename,'basename')

    def test_get_item_by_name(self):
        path = os.path.join('some_path', 'basename')
        tested_bucket_item = BucketItem('unique_name_in_bucket1', path, size_in_bytes=1, index=0)
        self.tested_manager.write_item(tested_bucket_item, 'unique_name_in_bucket1')
        result = self.tested_manager.get_item_by_basename(os.path.basename(path))
        self.assertEqual(tested_bucket_item, result)

if __name__ == '__main__':
    unittest.main()