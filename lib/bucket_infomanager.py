# -*- coding: utf-8 -*-
"""
This module contain class, which work with bucket info: write, read, delete
information about removed items

"""

import datetime
import os
from ConfigParser import ConfigParser

from smart_remover.lib import bucket_errors as err
from smart_remover.lib.bucket_item import BucketItem


class BucketInfoManager(object):
    """Objects of a class work with bucket info: write, read, delete
information about removed items

    Args:
        info_file (str): File, holding for storing data
    Attributes:
        info_file (str): File, holding for storing data

    """

    def __init__(self, info_file):
        self.info_file = info_file
        with open(self.info_file, "a"):
            pass

    def write_item(self, bucket_item,section_name):
        """Write bucket item in file

        Args:
            bucket_item (BucketItem): Bucket item for writing in file
            section_name (str): Section in file in which bucket item will be store

        Returns:
            None

        """

        def string_iso_from_datetime(datetime_for_write):
            return datetime.datetime.strftime(datetime_for_write, '%Y-%m-%dT%H:%M:%S.%f')

        parser = ConfigParser()
        parser.add_section(bucket_item.name_in_bucket)
        parser.set(section_name, 'old_path', bucket_item.old_path)
        parser.set(section_name, 'name_in_bucket', bucket_item.name_in_bucket)
        date = string_iso_from_datetime(bucket_item.deletion_date)
        parser.set(section_name,'deletion_date', date)
        parser.set(section_name, 'size_in_bytes', bucket_item.size_in_bytes)

        #with open(metafile, 'a') as metafile:
         #   metafile.write()

        with open(self.info_file, 'a') as parserfile:
            parser.write(parserfile)

    def get_item_by_index(self, index):
        """Read bucket item from file by index

        Args:
            index (int): Index of item

        Returns:
            BucketItem: return bucket item with given index

        """

        all_items = self.get_all_items_in_range()
        return all_items[index]

    def remove_item(self,section_name):
        """Remove bucket item info from file

        Args:
            section_name (str): Section of bucket item in file

        Returns:
            None

        """

        parser = ConfigParser()
        with open(self.info_file, "r") as parserfile:
            parser.readfp(parserfile)

        parser.remove_section(section_name)
        with open(self.info_file, "w") as parserfile:
            parser.write(parserfile)

    def clear_content(self):
        """Clear infofile content

        Returns:
            None

        """

        with open(self.info_file, "w") as f:
            pass

    def get_item_by_unique_name(self, unique_name):
        """Read bucket item from file by its unique name in bucket

        Args:
            unique_name (str): Unique name of item

        Returns:
            BucketItem: return bucket item with given unique name

        """

        def parse_iso_datetime(s):
            return datetime.datetime.strptime(s, '%Y-%m-%dT%H:%M:%S.%f')

        parser = ConfigParser()
        with open(self.info_file, "r") as parserfile:
            parser.readfp(parserfile)

        if not parser.has_section(unique_name):
            raise err.BucketDoesNotContainItemError(unique_name)

        old_path = parser.get(unique_name, 'old_path')
        name_in_bucket = parser.get(unique_name, 'name_in_bucket')
        deletion_date = parse_iso_datetime(parser.get(unique_name, 'deletion_date'))
        size_in_bytes = parser.getfloat(unique_name, 'size_in_bytes')
        return BucketItem(name_in_bucket, old_path, size_in_bytes, deletion_date)


    def get_all_items_in_range(self, start_index=None, last_index=None):
        """Get bucket items in range

        Args:
            start_index (index): Start index of items to return
            last_index (index): Last index of items to return

        Returns:
            list: return bucket items in given range

        """

        parser = ConfigParser()
        with open(self.info_file, "r") as parserfile:
            parser.readfp(parserfile)
        
        items = []
        for index,section in enumerate(parser.sections()[start_index:last_index]):
            items.append(self.get_item_by_unique_name(section))
            items[index].index = index
            if start_index is not None:
                items[index].index+=start_index
        return items

    def get_count(self):
        """Get count of bucket items

        Returns:
            int: return count of bucket items

        """

        return len(self.get_all_items_in_range())

    def get_total_items_size(self):
        """Get bucket items total size

        Returns:
            list: return bucket items total size

        """

        result = 0
        for item in self.get_all_items_in_range():
            result += item.size_in_bytes
        return result

    def get_all_items_by_basename(self, basename):
        """Get bucket items with given basename

        Args:
            basename (str): basename of items to get

        Returns:
            list: bucket items with given basename

        """

        items = self.get_all_items_in_range()
        marching_items = []
        for item in items:
            item_old_path_basename = os.path.basename(item.old_path)
            if item_old_path_basename == basename:
                marching_items.append(item)
        return marching_items

    def get_item_by_basename(self, basename):
        """Get bucket item with given basename

        Args:
            basename (str): basename of item to get

        Returns:
            BucketItem: bucket item with given basename

        Raises:
            BucketDoesNotContainItemError: if no info about bucket item with given basename
            NotSpecifiedItemError: if there are several bucket items with given basename

        """

        items_with_given_name = self.get_all_items_by_basename(basename)
        if (len(items_with_given_name) is 0):
            raise err.BucketDoesNotContainItemError(basename)
        if not (len(items_with_given_name) is 1):
            raise err.NotSpecifiedItemError(basename)  # TODO Exception several names march to base name
        return items_with_given_name[0]
#    def get_first_with_name(self, base_name):
            
            
        

