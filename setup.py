from setuptools import setup, find_packages
from os.path import join, dirname
import utility
import unittest

# def get_test_suite():
#     test_loader = unittest.TestLoader()
#     test_suite = test_loader.discover('tests', pattern='test_*.py')
#     return test_suite
#

setup(
    name='smart_rm',
    version=utility.__version__,
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    test_suite='smart_remover.tests',
    entry_points={
            'console_scripts':
                ['smart_rm = smart_remover.utility.smart_rm_main:main']
            }

)


