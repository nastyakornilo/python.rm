# -*- coding: utf-8 -*-
"""This module contains console implementations for bucket abstract infrastructure classes"""

from smart_remover.lib.abstract_interaction import ResultWriter
from smart_remover.lib.abstract_interaction import Querist


class ConsoleResultWriter(ResultWriter):
    """Class for writing in console results of bucket work"""

    def write_removed(self, removed_bucket_item):
        print "Removed: \n{}".format(removed_bucket_item)

    def write_clear(self, removed_bucket_item):
        print "Cleared: \n{}".format(removed_bucket_item)

    def write_clean(self):
        print "Clean \n"

    def write_restored(self, removed_bucket_item):
        print "Restored: \n{}".format(removed_bucket_item)

class ConsoleQuerist(Querist):

    def query_yes_no(self, question, default="yes"):
        """
        "default" is the answer if the user just hits <Enter>.
        """
        valid = {"yes": True, "y": True,
                 "no": False, "n": False}
        if default is None:
            prompt = " [y/n] "
        elif default == "yes":
            prompt = " [Y/n] "
        elif default == "no":
            prompt = " [y/N] "
        else:
            raise ValueError("invalid default answer: {}".format(default))

        while True:
            print (question + prompt)
            choice = raw_input().lower()
            if default is not None and choice == '':
                return valid[default]
            elif choice in valid:
                return valid[choice]
            else:
                print("Please respond with 'yes'/'no'/'y'/'n').\n")

    def want_del_file(self, file_name):
        return self.query_yes_no('remove file {}?'.format(file_name))

    def enter_dir(self, dir_name):
        return self.query_yes_no('descend into directory {}?'.format(dir_name))

    def want_del_empty_dir(self, dir_name):
        return self.query_yes_no('remove empty directory {}?'.format(dir_name))

    def want_clear_by_basename(self, base_name):
        return self.query_yes_no('clear by base name {}?'.format(base_name))

    def want_clear_all(self):
        return self.query_yes_no('clear all?')

    def want_clear_by_index(self, index):
        return self.query_yes_no('clear by index {}?'.format(index))

    def want_restore_by_index(self, index):
        return self.query_yes_no('restore by index {}?'.format(index))

    def want_restore_by_basename(self, base_name):
        return self.query_yes_no('restore by index basename {}?'.format(base_name))


# class ConsoleProgressWriter(ProgressWriter):
#     def write(self, done, total):
#         if not total == 0:
#             per_cent = done / total
#             print "Operation progress: {} %".format(per_cent)
#
