# -*- coding: utf-8 -*-
"""This module contain function for creating dict config"""
import dateparser
import os
from smart_remover.utility.policy import AutocleanType
import json
import datetime
import itertools
import logging
import argparse
from smart_remover.utility.action_manager import Action


WEEK_AGO = datetime.date.today() - datetime.timedelta(days=7)

DEFAULT_CONFIG = {
    'bucket': {
        'bucket_dir': os.path.join(os.environ['HOME'], '.smart_rm/trash'),
        'trashfiles_dir_name': 'files',
        'infofile': 'trashfile.info',

        'max_bucket_size_in_bytes': 1024*1024*1024
    },
    'mode': {
        'silent': False,
        'interactive': False,
        'verbose': False,
        'recursive': False,
        'directory': False,
        'dry_run': False
    },
    #'action_type':Action.remove,
    'logger':{
        'log_file': os.path.join(os.environ['HOME'], '.smart_rm/log.file'),
        'log_level': "DEBUG",
        'log_in_console': True
    },
    #'files': None,
    'policy':{
        'earliest_deletion_time': WEEK_AGO,
        'autoclean_type': AutocleanType.no_autoclean
    }
}

def get_extension(file_path):
    """Method for getting extension of a file

    Args:
        file_path: Path of file for getting extension

    Returns:
        str: Return extension of a file (including point)

    """

    file_basename = os.path.basename(file_path)
    return os.path.splitext(file_basename)[-1]


def get_dict_config_from_file(config_file):
    """Function for reading dict config from file

    Args:
        config_file: File with config dict.

    Returns:
        dict: Return config

    """

    if config_file is None:
        return DEFAULT_CONFIG
    try:
        config_file = os.path.abspath(config_file)
        file_extension = get_extension(config_file)

        if file_extension == '.json':
            return get_dict_config_from_json_file(config_file)
    except IOError:
        print 'Configuration file not found'
    except ValueError:
        print 'Invalid configuration file'


def get_dict_config_from_json_file(config_file):
    """Function for getting dict config from file holding
    information in json format

        Args:
            config_file: File with configuration data.

        Returns:
            dict: Return config

        """

    # with open(config_file, 'r') as file:
    #     dict_config = jsonpickle.decode(file.read(), unpicklable=False)
    # return dict_config
    #json.JSONEncoder.object_hook = lambda self, obj: datetime.datetime.strptime(obj)

    def date_hook(json_dict):
        for (key, value) in json_dict.items():
            if key == 'earliest_deletion_time':
                json_dict[key] = dateparser.parse(value)
            else:
                json_dict[key] = value
        return json_dict

    with open(config_file, 'r') as config_file:
        return json.load(config_file, object_hook=date_hook)#json_util.object_hook)


def get_dict_config(config):
    """Function for getting dict config from object
        containig necessary for configuration fields

    Args:
        config: Object with necessary fields

    Returns:
        dict: Return config

    """

    config_dict = dict()
    config_dict['bucket'] = dict()
    config_dict['mode'] = dict()
    config_dict['logger'] = dict()
    config_dict['action_type'] = Action.remove
    config_dict['action_params'] = dict()
    config_dict['policy'] = dict()
    config_dict['logger'] = dict()
    config_dict['files'] = config.files

    config_dict['config'] = config.config_file
    if config.bucket_dir is not None:
        config_dict['bucket']['bucket_dir'] = config.bucket_dir
    if config.trashfiles_dir_name is not None:
        config_dict['bucket']['trashfiles_dir_name'] = config.trashfiles_dir_name

    if config.infofile is not None:
        config_dict['bucket']['infofile'] = config.infofile
    if config.max_bucket_size_in_bytes is not None:
        config_dict['bucket']['max_bucket_size_in_bytes'] = config.max_bucket_size_in_bytes

    if config.silent:
        config_dict['mode']['silent'] = config.silent
    if config.interactive:
        config_dict['mode']['interactive'] = config.interactive
    if config.verbose:
        config_dict['mode']['verbose'] = config.verbose
    if config.recursive:
        config_dict['mode']['recursive'] = config.recursive
    if config.directory:
        config_dict['mode']['directory'] = config.directory
    if config.dry_run:
        config_dict['mode']['dry_run'] = config.dry_run
    if config.force:
        config_dict['mode']['force'] = config.force

    if config.log_level is not None:
        config_dict['logger']['log_level'] = config.log_level
    if config.log_file is not None:
        config_dict['logger']['log_file'] = config.log_file
    if config.log_in_console is not None:
        config_dict['logger']['log_in_console'] = config.log_in_console

    if config.autoclean_policy_type is not None:
        config_dict['policy']['autoclean_type'] = config.autoclean_policy_type
    if config.earliest_deletion_time is not None:
        config_dict['policy']['earliest_deletion_time'] = dateparser.parse(config.earliest_deletion_time)

    if config.clean:
        config_dict['action_type'] = Action.clean
    if config.regex is not None:
        config_dict['action_type'] = Action.remove_regex
        config_dict['action_params']['regex'] = config.regex
    if config.range_of_items_to_view is not None:
        if config.range_of_items_to_view == 'all':
            config_dict['action_params']['start_index'] = None
            config_dict['action_params']['last_index'] = None
        else:
            range_items_to_view = config.range_of_items_to_view.split("-")
            config_dict['action_params']['start_index'] = int(range_items_to_view[0])
            config_dict['action_params']['last_index'] = int(range_items_to_view[-1])
        config_dict['action_type'] = Action.view
    if config.restore_by_base_name:
        config_dict['action_type'] = Action.restore_by_basename
    if config.item_to_restore_index is not None:
        config_dict['action_type'] = Action.restore_by_index
        config_dict['action_params']['index']=config.item_to_restore_index
    if config.item_to_clear_index is not None:
        config_dict['action_type'] = Action.clear_by_index
        config_dict['action_params']['index'] = config.item_to_clear_index
    if config.clear_by_base_name:
        config_dict['action_type'] = Action.clear_by_basename

    if config.log_file is not None:
        config_dict['logger']['log_file'] = config.log_file

    if config.log_level is not None:
        config_dict['logger']['log_level'] = config.log_level
    return config_dict


def merge_dict_configs(preferable_config, other_config=DEFAULT_CONFIG):
    """Function for recursive merging two dict configs

        Args:
            preferable_config: Dict, fields of which will be preferable in coflicts
            other_config: Dict config mor merging with preferable dict

        Returns:
            dict: Return maximum possible size dict with collision key-value-pairs
            gotten from preferable dict

        """

    merged_config = dict()
    for key, value in itertools.chain(other_config.items(), preferable_config.items()):
        if isinstance(value, dict):
            if key in merged_config:
                merged_config[key] = merge_dict_configs(value, merged_config[key])
            else:
                merged_config[key] = value
        else:
            merged_config[key] = value

    return merged_config



def configure_logger(log_file, log_level=logging.INFO, log_in_console=False):
    """Configure logger

    Args:
        log_file: file for logging
        log_level: level of logging
        log_in_console: is need log in console

    Returns:
        logger

    Raises:
         ValueError: if log_level does not correspond any allowed
            logging levels (as string or enumeration from module logging)

    """

    logger = logging.getLogger('')
    # self.logger = logging.getLogger('log.config')

    numeric_level = getattr(logging, log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % log_level)

    logger.setLevel(numeric_level)

    formatter = logging.Formatter(
        u'%(filename)s[LINE:%(lineno)d]# %(asctime)s - %(name)s - %(levelname)s - %(message)s')

    filehandler = logging.FileHandler(log_file)
    filehandler.setLevel(numeric_level)
    filehandler.setFormatter(formatter)
    logger.addHandler(filehandler)

    if log_in_console:
        console_handler = logging.StreamHandler()
        console_handler.setLevel(numeric_level)
        console_handler.setFormatter(formatter)
        logger.addHandler(console_handler)

    # logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
    #                   level=logging.DEBUG)

    return logger


def get_arguments():
    """Get console arguments

    Returns:
        namespace: arguments from console

    """

    parser = argparse.ArgumentParser(description="This is the smart remover utility")

    mode = parser.add_mutually_exclusive_group()

    remove_mode = mode.add_argument_group()
    remove_mode.add_argument('--dir', '-d', action="store_true", dest='directory',help='remove empty directories')
    remove_mode.add_argument('--recursive', '-r',action="store_true", dest='recursive',
                             help='remove directories and their contents recursively'
                             )
    mode.add_argument('--regex', '-g',dest='regex', type=str, action='store',
                      help='remove recursively from regular expression'
                      )

    mode.add_argument('-w', '--view', dest='range_of_items_to_view', type=str,
                      help='viewing the contents of the trash by range'
                      )

    mode.add_argument('-e', '--restore_by_basename', action='store_true', dest='restore_by_base_name',
                      help='restore from bucket by basename'
                      )

    mode.add_argument('-E', '--restore_by_index',dest='item_to_restore_index', type=int, action='store',
                      help='restore files from the bucket by index'
                      )

    mode.add_argument('-c','--clear_by_basename', action='store_true', dest='clear_by_base_name',
                      help='clear from bucket by basename'
                      )

    mode.add_argument('-C', '--clear_by_index', dest='item_to_clear_index', type=int, nargs='+',
                      help='clear from bucket by index'
                      )
    mode.add_argument('-n', '--clean', action="store_true", dest='clean',help='clean bucket content')

    parser.add_argument('-f','--force',
                             action="store_true", dest='force',
                             help='ignore nonexistent files and arguments, never prompt'
                        )
    parser.add_argument('-i','--interactive',action="store_true", dest='interactive',help='prompt before every removal')
    parser.add_argument('-v','--verbose',action="store_true", dest='verbose',help="explain what is being done")
    parser.add_argument('-s','--silent', action="store_true", dest='silent', help="returning code")
    parser.add_argument('-y','--dry_run',action="store_true", dest='dry_run',
                        help="Dry run mode. Only emulating operation execution"
                        )

    parser.add_argument('--bucket_dir', type=str, nargs='?', dest='bucket_dir', help='path to bucket')
    parser.add_argument('--bucket_infofile', type=str, nargs='?', dest='infofile',
                        help='relative path in bucket for directory with infofiles'
                        )
    parser.add_argument('--bucket_trashfiles_dir', type=str, nargs='?', dest='trashfiles_dir_name',
                        help='relative path in bucket for directory with trashfiles'
                        )
    parser.add_argument('--max', action="store", default=None, dest='max_bucket_size_in_bytes',
                        help='Max bucket size'
                        )
    #parser.add_argument('--name_policy',
     #                   action="store", default=None, dest='equal_name_policy',
      #                  help='policy for case with name collision'
       #                 )
    parser.add_argument('-a','--autoclean_policy',action="store", default=None, dest='autoclean_policy_type',
                        help='policy which defines when do auto-cleaning'
                        )
    parser.add_argument('files', nargs=argparse.REMAINDER,
                        help='files for removing, restoring or clearing(depends on mode)'
                        )

    #nargs=argparse.REMAINDER


    parser.add_argument('-l', '--logfile', dest='log_file', type=str, nargs='?',help='log file')
    parser.add_argument('--log_level', dest='log_level', type=str, help='logging level')
    parser.add_argument('--log_in_console', action = "store_true", dest='log_in_console', help='log_in_console')
    parser.add_argument('-o', '--config', dest='config_file', type=str, nargs='?', help='configuration file')
    parser.add_argument('-t', '--time', dest='earliest_deletion_time', type=str, nargs='?',
                        help='time; files, which were deleted earlier than that time will be clear'
                        )
    namespace = parser.parse_args()
    # print vars(namespace)
    #
    # command_line_args = {k:v for k,v in vars(namespace).iteritems() if v}
    #
    # print command_line_args

    return parser.parse_args()


# def merge_dict_config(preferable_dict, other_dict=DEFAULT_CONFIG):
#     merged_dict = other_dict.copy()
#     for key in merged_dict.iterkeys():
#         if isinstance(merged_dict[key], dict):
#             merged_dict[key].update(preferable_dict[key])
#         else:
#             if preferable_dict[key]:
#                 merged_dict[key]  = preferable_dict[key]
#     return merged_dict
# def check_is_dict_config_valid(config):
#     def check_is_params_in_subdict(params_list, subdict_config):
#         for param in params_list:
#             if not (param in subdict_config):
#                 return False
#         return True
#
#     def check_bucket_config():
#         if not ('bucket' in config):
#             return False
#         bucket_params = ['bucket_dir', 'trashfiles_dir_name', 'infofile', 'max_bucket_size_in_bytes']
#         return check_is_params_in_subdict(bucket_params, config_dict['bucket'])
#
#     def check_mode_config():
#         if not ('mode' in config):
#             return False
#         modes = ['silent', 'interactive', 'verbose', 'recursive', 'directory', 'dry_run']
#         return check_is_params_in_subdict(modes, config_dict['mode'])
#
#     def check_logger_config():
#         if not ('logger in config' in config):
#             return False
#         logger_params = ['log_file', 'log_level', 'log_in_console']
#         return check_is_params_in_subdict(logger_params, config_dict['logger'])
#
#     def check_policy_config():
#         if 'policy' not in config:
#             return False
#         if 'autoclean_type' not in config_dict['policy']:
#             return False
#         if config_dict['policy']['autoclean_type'] == AutocleanType.combined or \
#                         config_dict['policy']['autoclean_type'] == AutocleanType.old_files:
#             return False
#         return True
#
#     if 'action_type' not in config:
#         return False
#
#     return check_bucket_config() and check_logger_config() and check_mode_config() and check_policy_config()

# def get_dict_config_from_config_parser_file(config_file):
#     """Function for getting dict config from file holding
#     information in config parser format
#
#     Args:
#         config_file: File with configuration data.
#
#     Returns:
#         dict: Return config
#
#     """
#
#     config = ConfigParser()
#     with (open(config_file, 'r')) as file:
#         config.read(file)
#     return {section:dict(config.items(section)) for section in config.sections()}
#
