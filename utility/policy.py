# -*- coding: utf-8 -*-
"""This module contain implementation for abstract bucket policy class"""
import os
import datetime
import prettytable
from smart_remover.lib.abstract_policy import AbstractPolicy
from smart_remover.lib import bucket_errors as err


class AutocleanType(object):
    """Class represent type of autoclean policies"""

    no_autoclean ='no'
    overflow = 'overflow'
    old_files = 'old files'
    combined = 'combined'

# class AutocleanUnnecessaryFilesTime(object):
#     hour = 'one_hour'
#     eight_hours = 'eight_hours'
#     day = 'day'
#     week = 'week'
#     month = 'month'
#     year = 'year'
#
#     @staticmethod
#     def time(self, string_time):
#         if string_time == AutocleanUnnecessaryFilesTime.hour:
#             return datetime.date.today() - datetime.timedelta(hours=1)
#         if string_time == AutocleanUnnecessaryFilesTime.hour:
#             return datetime.date.today() - datetime.timedelta(hours=8)
#         if string_time == AutocleanUnnecessaryFilesTime.week:
#             return datetime.date.today() - datetime.timedelta(weeks=1)
#         if string_time == AutocleanUnnecessaryFilesTime.month:
#             return datetime.date.today() - datetime.timedelta(days=30)
#         if string_time == AutocleanUnnecessaryFilesTime.week:
#             return datetime.date.today() - datetime.timedelta(days=7)
#         raise TypeError
#
class Policy(AbstractPolicy):
    """This class provide interface for inheritance in bucket policy classes

    Args:
        autoclean_type (AutocleanType): type of autoclean policy
        earliest_deletion_time (datetime): time, files with deletion time earlier of which will cleared
    Attributes:
        autoclean_type (AutocleanType): type of autoclean policy
        earliest_deletion_time (datetime): time, files with deletion time earlier of which will cleared

    """
    def __init__(self,
                 autoclean_type=None,
                 earliest_deletion_time=None
                 ):
        self.earliest_deletion_time = earliest_deletion_time
        if autoclean_type is None:
            self.autoclean_type = AutocleanType.no_autoclean
        else:
            self.autoclean_type = autoclean_type

    # def autoclean(self, bucket):
    #     now = datetime.datetime.now()
    #     autoclean_time = now.replace(hour=self.autoclean_hour)
    #     if now - autoclean_time < datetime.timedelta(hours=1):
    #         return True
    #     return False
    def autoclean(self, instance, method):
        """Autoclean decorator

        Args:
             method: method for decorating

        Returns:
            method: method decorating with autoclean action

        Raises:
            TypeError: if self.earliest_deletion_time is None and autoclean type
                is combined or old_files
        """

        if self.autoclean_type == AutocleanType.no_autoclean:
            return method
        if self.autoclean_type == AutocleanType.overflow:
            return self._autoclean_overflow(instance,method)

        if self.earliest_deletion_time is None:
            raise TypeError
        if self.autoclean_type == AutocleanType.old_files:
            return self._autoclean_old_files(instance, method)
        if self.autoclean_type == AutocleanType.combined:
            return self._autoclean_old_files(instance, self._autoclean_overflow(instance, method))

    def specify_item(self, items):
        """Policy for solving not specified item error

        Args:
             items: items from which need to choose one item

        Returns:
            BucketItem: chosen item or None

        """

        table = prettytable.PrettyTable(['Index', 'Path', 'Deletion date'])

        for item in items:
            table.add_row([item.index, item.old_path, item.deletion_date])
        print table
        chosen_index = int(raw_input('Please, specify name, choosing index: '))
        for index,item in enumerate(items):
            if chosen_index == item.index:
                return items[index]
        return None

    def solve_path_collision(self, path):
        """Policy for solving collision of paths

        Args:
             path (str): collision path

        Returns:
            str: path with solved collision

        """

        new_path = path
        counter = 1
        while os.path.exists(new_path):
            new_path = '{}_({})'.format(path,counter)
            print new_path
            counter += 1
        return new_path

    def _autoclean_overflow(self, inner_self, method):
        def inner(*args, **kwargs):
            try:
                return method(*args, **kwargs)
            except err.NotEnoughFreeSpaceException:
                inner_self.clean()
                return method(*args, **kwargs)

        return inner

    def _autoclean_old_files(self, inner_self, method):
        def inner(*args, **kwargs):
            items = inner_self.items
            for item in items:
                if item.deletion_date < self.earliest_deletion_time:
                    inner_self.clear_by_name_in_bucket(item.name_in_bucket)
                    # self.clear_by_base_name
            return method(*args, **kwargs)

        return inner


            #
# def _save_last_call(function):
#     def inner(*args, **kwargs):
#         inner.last_call = datetime.datetime.now()
#         return function(*args, **kwargs)
#     inner.last_call = datetime.datetime.now()
#     return inner
#


#def last_call_autoclean_condition(self, bucket):

#def free_space_autoclean_condition(self, bucket):
 #   pass

    #if bucket.space_size-bucket.used_space_size


