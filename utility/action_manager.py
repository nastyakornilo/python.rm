# -*- coding: utf-8 -*-
"""
This module contain class ActionManager for easy running
any method from bucket, tools for pretty representation bucket data

"""
import os
import prettytable


class Action(object):
    """Class represent type of actions for execute"""

    remove = 0
    remove_regex = 1
    restore_by_basename = 2
    restore_by_index = 3
    clear_by_basename = 4
    clear_by_index = 5
    view = 6
    clean = 7

class ActionManager(object):
    """Objects of a class work manipulate bucket methods

    Args:
        bucket: Bucket for manipulating
        action_type(Action): type of action to execute

    Attributes:
        bucket: Bucket for manipulating
        action_type(Action): type of action to execute
    """

    def __init__(self,
                 bucket,
                 action_type = Action.remove,
                 ):
        self.bucket = bucket
        self.action_type = action_type

    def action(self,
               index=None,
               start_index=None,
               last_index=None,
               regex=None,
               files=None
               ):
        """Do one of bucket actions

        Args:
             index (int): index for bucket operation with index
            start_index (int): start index for showing bucket content
            last_index: finish index for showing bucket content
            regex: regex for searching removable files
            files: files for executing operations

        Returns:
            object: result of bucket action

        """

        if self.action_type == Action.remove:
            return self._do_with_files_list(self.bucket.remove, files)
        if self.action_type == Action.remove_regex:
            return self._do_with_files_list(self.bucket.remove_tree_by_regex, files, regex)
        if self.action_type == Action.restore_by_index:
            return self.bucket.restore_by_index(index)
        if self.action_type == Action.restore_by_basename:
            return self._do_with_files_list(self.bucket.restore_by_basename, files)
        if self.action_type == Action.clear_by_index:
            return self.bucket.clear_by_index(index)
        if self.action_type == Action.clear_by_basename:
            return self._do_with_files_list(self.bucket.clear_by_basename, files)
        if self.action_type == Action.view:
            content = self.bucket.show_content(start_index, last_index)
            print_items_in_table(content)
            return content
        if self.action_type == Action.clean:
            return self.bucket.clean()

    def _do_with_files_list(self, action, files, regex=None):
        result = []
        for file in files:
            if self.action_type == Action.remove or self.action_type == Action.remove_regex:
                file = os.path.abspath(file)
            elif self.action_type == Action.restore_by_basename or self.action_type == Action.clear_by_basename:
                file = os.path.basename(file)
            if self.action_type == Action.remove_regex:
                if regex is None:
                    raise TypeError('Removing by regex. Regex can not be None')
                result.append(action(file, regex))
            else:

                result.append(action(file))
        return result


def print_items_in_table(items):
    """Function for printing list of bucket items in table

        Args:
            items: list of bucket items

        Returns:
            None: None

        """

    table = prettytable.PrettyTable(['Index', 'Path', 'Deletion date'])
    for item in items:
        table.add_row([item.index, item.old_path, item.deletion_date])
    print table

