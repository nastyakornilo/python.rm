# -*- coding: utf-8 -*-
"""This module classes provide interface for inheritance in bucket policy classes"""
import abc


class AbstractPolicy(object):
    """This class provide interface for inheritance in bucket policy classes"""

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def autoclean(self,instance, method):
        pass

    @abc.abstractmethod
    def specify_item(self,all_items_with_name):
        pass

    @abc.abstractmethod
    def solve_path_collision(self,path):
        pass