import unittest
from smart_remover.lib.bucket_item import BucketItem
from smart_remover.utility.action_manager import ActionManager
from smart_remover.utility.action_manager import Action
import mock
import os

class MockBucket(object):
    def __init__(self):
        first_arg_returning_func = mock.Mock(side_effect=lambda arg, *args, **kwargs: arg)
        self.remove = first_arg_returning_func
        self.remove_tree_by_regex = first_arg_returning_func
        self.restore_by_basename = first_arg_returning_func
        self.restore_by_index = mock.Mock()
        self.clear_by_index = mock.Mock()
        self.clear_by_basename = first_arg_returning_func
        self.show_content = mock.Mock(side_effect=lambda start_index, last_index: (start_index, last_index))
        self.clean = mock.Mock(side_effect=lambda: None)

def create_paths_list_for_action(list_length):
    return [os.path.abspath(name) for name in create_basenames_list_for_action(list_length)]


def create_basenames_list_for_action(list_length):
    return ['name{}'.format(i) for i in range(list_length)]

FILES_COUNT = 3

class ActionManagerTestCase(unittest.TestCase):
    def setUp(self):
        self.mock_bucket = MockBucket()
        self.action_manager = ActionManager(self.mock_bucket)

    def test_action_type_remove(self):
        self.action_manager.action_type = Action.remove
        files = create_paths_list_for_action(FILES_COUNT)

        action_result = self.action_manager.action(files=files)
        self.assertEqual(self.mock_bucket.remove.call_count, FILES_COUNT)
        self.assertListEqual(action_result, files)

    def test_action_type_remove_regex(self):
        self.action_manager.action_type = Action.remove_regex
        files = create_paths_list_for_action(FILES_COUNT)
        regex = 'regex'

        action_result = self.action_manager.action(files=files, regex=regex)
        self.assertEqual(self.mock_bucket.remove_tree_by_regex.call_count, FILES_COUNT)
        self.assertListEqual(action_result, files)

    def test_action_type_remove_regex_raise_type_error(self):
        self.action_manager.action_type = Action.remove_regex
        regex = None
        files = create_paths_list_for_action(FILES_COUNT)

        self.assertRaises(TypeError, self.action_manager.action, files, regex)


    def test_action_type_restore_by_basename(self):
        self.action_manager.action_type = Action.restore_by_basename
        basenames = create_basenames_list_for_action(FILES_COUNT)

        action_result = self.action_manager.action(files=basenames)
        self.assertEqual(self.mock_bucket.restore_by_basename.call_count, FILES_COUNT)
        self.assertListEqual(action_result, basenames)

    def test_action_type_restore_by_index(self):
        self.action_manager.action_type = Action.restore_by_index
        restore_item_index = 8
        restore_by_index_result = 186

        self.mock_bucket.restore_by_index = mock.Mock(side_effect=lambda index: restore_by_index_result)

        action_result = self.action_manager.action(index=restore_item_index)
        self.mock_bucket.restore_by_index.assert_called_once_with(restore_item_index)
        self.assertEqual(action_result, restore_by_index_result)

    def test_action_type_clear_by_basename(self):
        self.action_manager.action_type = Action.restore_by_basename
        basenames = create_basenames_list_for_action(FILES_COUNT)

        action_result = self.action_manager.action(files=basenames)
        self.assertEqual(self.mock_bucket.clear_by_basename.call_count, FILES_COUNT)
        self.assertListEqual(action_result, basenames)

    def test_action_type_clear_by_index(self):
        self.action_manager.action_type = Action.clear_by_index
        clear_item_index = 43  # random index for test
        clear_by_index_result = 126  # random index for test

        self.mock_bucket.clear_by_index = mock.Mock(side_effect=lambda index: clear_by_index_result)

        action_result = self.action_manager.action(index=clear_item_index)
        self.mock_bucket.clear_by_index.assert_called_once_with(clear_item_index)
        self.assertEqual(action_result, clear_by_index_result)

    def test_action_type_clean(self):
        self.action_manager.action_type = Action.clean

        action_result = self.action_manager.action()
        self.mock_bucket.clean.assert_called_once_with()
        self.assertTrue(action_result is None)

    def test_action_type_show_content(self):
        self.action_manager.action_type = Action.view
        start_index = 1
        last_index = 2
        show_content_result = [BucketItem('name_in_bucket', 'old_path', 5)]
        self.mock_bucket.show_content = mock.Mock(return_value=show_content_result)
        action_result = self.action_manager.action(start_index=start_index, last_index=last_index)
        self.mock_bucket.show_content.assert_called_once_with(start_index, last_index)
        self.assertListEqual(action_result, show_content_result)


if __name__ == '__main__':
    unittest.main()