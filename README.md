# SmartRM v1.0 #
### Author: Kornilo Anastasia
### Email: nastyakornilo2@gmail.com

### Description :###
 Smart utility for remove, restore files and directories

### Installation ###

```
#!bash

> sudo python setup.py install
```
###Running tests###
 
```
#!bash

>python setup.py test
```


### Using ###

```
#!bash

>smart_rm [ARGS] [FILES]
```

###Command line arguments###

*positional arguments:*

*   files - files for removing, restoring or clearing

*optional arguments:*

 *  -h, --help - Help message
 *   --regex REGEX [REGEX ...], -g REGEX [REGEX ...] - Remove recursively from regular 
 expression
 *   -w RANGE_OF_ITEMS_TO_VIEW, --view RANGE_OF_ITEMS_TO_VIEW - Viewing the contents of the 
 trash by range

     RANGE_OF_ITEMS_TO_VIEW possible values:

     *   all
     *   1-10 (start_index - end_index)

 *   -e, --restore_by_basename - restore from bucket by basename
 *   -E ITEM_TO_RESTORE_INDEX, --restore_by_index ITEM_TO_RESTORE_INDEX - Restore files 
 from the bucket by index
 *   -c, --clear_by_basename - clear from bucket by basename
 *   -C ITEM_TO_CLEAR_INDEX, --clear_by_index ITEM_TO_CLEAR_INDEX - Clear from bucket by 
 index
 *   -n, --clean - Clean bucket content
 *   -f, --force - Ignore nonexistent files and arguments, never prompt
 *   -i, --interactive - Prompt before every removal
 *   -v, --verbose - Explain what is being done
 *   -s, --silent - Returning code
 *   -y, --dry_run - Dry run mode. Only emulating operation execution
 *   --bucket_dir [BUCKET_DIR] - Path to bucket
 *   --bucket_infofile [INFOFILE] - Name for directory with infofiles
 *   --bucket_trashfiles_dir [TRASHFILES_DIR_NAME] - Name directory with trashfiles
 *   --max MAX_BUCKET_SIZE_IN_BYTES - Max bucket size
 *   -a AUTOCLEAN_POLICY_TYPE, --autoclean_policy AUTOCLEAN_POLICY_TYPE - Policy which 
 defines when do auto-cleaning

     AUTOCLEAN_POLICY_TYPE possible values:

     *   no
     *   overflow
     *   old_files
     *   combined

 *   -t [EARLIEST_DELETION_TIME], --time [EARLIEST_DELETION_TIME] - Time; files, which were 
 deleted earlier than that time will be clear
 *   -l [LOG_FILE], --logfile [LOG_FILE] - Log file
 *   --log_level LOG_LEVEL - Logging level

     LOG_LEVEL possible values:

     *   critical
     *   error	
     *   warning
     *   info	
     *   debug	
     *   notset

 *   --log_in_console - Log in console
 *   -o [CONFIG_FILE], --config [CONFIG_FILE] - Set configuration file