# -*- coding: utf-8 -*-
"""This module contain class-model for object, which is stored in bucket"""
from datetime import datetime


class BucketItem(object):
    """Objects of a class represent soring in bucket item

    Args:
        name_in_bucket (str): Unique name, under which item can be found in bucket directory
        old_path (str): previous item location
        size_in_bytes (int): removed file size in bytes
        deletion_date (datetime): date of deletion
        index: removed item index
    Attributes:
        name_in_bucket (str): Unique name, under which item can be found in bucket directory
        old_path (str): previous item location
        size_in_bytes (int): removed file size in bytes
        deletion_date (datetime): date of deletion
        index: removed item index
    """
    def __init__(self,
                 name_in_bucket,
                 old_path,
                 size_in_bytes,
                 deletion_date=None,
                 index=None
                 ):
        # type: (str,str,float,datetime)->None
        if deletion_date is None:
            self.deletion_date = datetime.now()
        else:
            self.deletion_date = deletion_date
        self.name_in_bucket = name_in_bucket
        self.old_path = old_path
        self.size_in_bytes = size_in_bytes
        self.index = index

    def __str__(self):
        # infofile will add current index....?
        #return str(self.__dict__)
        return "\tIndex:{} \n\tOriginal path:{} \n\tSize: {} \n\tDeletion date:{}\n".format(self.index,
                                                                                            self.old_path,
                                                                                            self.size_in_bytes,
                                                                                            self.deletion_date,
                                                                                            self.name_in_bucket
                                                                                            )

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.__dict__ == other.__dict__
